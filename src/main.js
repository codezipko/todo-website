import Vue from 'vue'
import Vuex from 'vuex'
import Router from 'vue-router'
import App from './App.vue'
import {routes} from "./core/rootRoutes";
import './assets/scss/main.scss';
import {store} from "./core/store/store";

Vue.use(Router)
Vue.use(Vuex)

Vue.config.productionTip = false

const router = new Router({
  mode: 'history',
  routes
})

new Vue({
  router,
  render: h => h(App),
  store,
}).$mount('#app')
