export const constants = {
    todos: {
        mutations: {
            GET: 'todos.GET',
            SET: 'todos.SET',
            ADD: 'todos.ADD',
            TODO_STATUS: 'todos.TODO_STATUS',
            SORT: 'todos.SORT',
            ORDER: 'todos.ORDER'
        }
    },

    user: {
        mutations: {
            LOGIN: 'user.LOGIN',
            SET: 'user.SET',
            STATUS: 'user.STATUS'
        }
    },

    users: {
        mutations: {
            CREATE: 'users.CREATE',
            GET: 'users.GET',
            STATUS: 'users.STATUS',
            DELETE: 'users.DELETE'
        }
    }
}