import Login from '../Pages/Login/Login'
import Register from '../Pages/Register/Register'
import Todos from '../Pages/Todos/Todos'
import Admin from '../Pages/Admin/Admin'

export const rootRoutes = {
    home: '/',
    login: {
        index: '/login',
        register: '/register'
    },
    admin: {
        index: '/admin',
    }
};

export const routes = [
    { path: rootRoutes.home, component: Todos },
    { path: rootRoutes.login.index, component: Login },
    { path: rootRoutes.login.register, component: Register },
    { path: rootRoutes.admin.index, component: Admin }
]