import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import Todos from '../modules/Todos/Todos'
import Users from '../modules/Users/Users'
import User from '../modules/User/User'

Vue.use(Vuex)

const persist = new VuexPersistence({
    storage: window.localStorage,
    key: 'User',
    reducer: (state) => state.User,
})

export const store = new Vuex.Store(
    {
        modules: {
            Todos,
            Users,
            User
        },
        plugins: [persist.plugin]
    }
)