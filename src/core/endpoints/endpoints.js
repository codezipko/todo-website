import {
    addTodoToList,
    getTodosList,
    updateTask
} from "./todos";

import {
    getUsersList,
    createUser,
    loginUser,
    updateUser,
    logoutUser,
    deleteUser
} from "./user";

export const baseURL = 'http://localhost/homework/backend/public/api';

export const defaultHeaders = {
    'Content-Type': 'application/json'
};

export const endpoints = {
    ...addTodoToList,
    ...getTodosList,
    ...updateTask,
    ...getUsersList,
    ...createUser,
    ...loginUser,
    ...updateUser,
    ...logoutUser,
    ...deleteUser
};