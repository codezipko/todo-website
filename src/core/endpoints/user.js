export const createUser = {
    CREATE_USER: {
        name: 'CREATE_USER',
        url: {
            method: 'post',
            path: () => `/register`,
        },
    },
};

export const loginUser = {
    LOGIN_USER: {
        name: 'LOGIN_USER',
        url: {
            method: 'post',
            path: () => `/login`,
        },
    },
};

export const getUsersList = {
    GET_USERS_LIST: {
        name: 'GET_USERS_LIST',
        url: {
            method: 'GET',
            path: () => `/users`,
        },
    },
};

export const updateUser = {
    UPDATE_USER: {
        name: 'UPDATE_USER',
        url: {
            method: 'PUT',
            path: () => `/user`,
        },
    }
};

export const deleteUser = {
    DELETE_USER: {
        name: 'DELETE_USER',
        url: {
            method: 'DELETE',
            path: (id) => `/user-delete/${id}`,
        },
    }
};

export const logoutUser = {
    LOGOUT_USER: {
        name : 'LOGOUT_USER',
        url: {
            method: 'PUT',
            path: () => `/logout`,
        }
    }
};