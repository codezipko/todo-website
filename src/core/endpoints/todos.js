export const addTodoToList = {
    CREATE_TODO: {
        name: 'CREATE_TODO',
        url: {
            method: 'POST',
            path: () => '/todo'
        }
    }
};

export const getTodosList = {
    GET_TODOS_LIST: {
        name: 'GET_TODOS_LIST',
        url: {
            method: 'GET',
            path: (status, order) => `/todos/${status}/${order}`,
        },
    },
};

export const updateTask = {
    UPDATE_TASK: {
        name: 'UPDATE_TASK',
        url: {
            method: 'GET',
            path: () => `/update-task`
        }
    }
}