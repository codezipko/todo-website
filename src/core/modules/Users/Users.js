import {constants} from '../../constants/constants'
import axios from 'axios'
import {endpoints, baseURL, defaultHeaders} from "../../endpoints/endpoints";

const state = {
    users: [],
    userStatus: ''
}

const getters = {}

const actions = {
    createUser({dispatch}, payload) {
        axios
            .post(baseURL + endpoints.CREATE_USER.url.path(), {
                username: payload.username,
                email: payload.email,
                password: payload.password,
                role: payload.role
            })
            .then(function (response) {
                if (response) {
                    dispatch('setUserStatus', 'CREATED')
                    dispatch('getUsers')
                }
            })
            .catch(function (error) {
                if (error.response.data === 'already_exists') {
                    dispatch('setUserStatus', 'USER_EXISTS')
                } else {
                    dispatch('setUserStatus', 'FAILED')
                }
            })
    },

    getUsers({commit, rootState}) {
        axios
            .get(baseURL + endpoints.GET_USERS_LIST.url.path(), {
                headers: {
                    defaultHeaders,
                    Authorization: `Bearer ${rootState.User.user.token}`
                }
            })
            .then(function (response) {
                if (response) {
                    commit(constants.users.mutations.GET, response.data)
                }
            })
            .catch(function (error) {
                if (error.response.status === 401) {
                    commit(constants.user.mutations.SET, {})
                    commit(constants.user.mutations.STATUS, false)
                    window.location.href = '/login'
                }
            })
    },

    setUserStatus({commit}, status) {
        commit(constants.users.mutations.STATUS, status)
    },

    deleteUser({dispatch, rootState}, payload) {
        axios
            .delete(baseURL + endpoints.DELETE_USER.url.path(payload.user_id), {
                headers: {
                    defaultHeaders,
                    Authorization: `Bearer ${rootState.User.user.token}`
                }
            })
            .then(function (response) {
                if (response) {
                    dispatch('getUsers')
                }
            })
    }
}

const mutations = {
    [constants.users.mutations.GET]
        (state, payload) {
        state.users = payload;
    },

    [constants.users.mutations.STATUS]
        (state, status) {
        state.userStatus = status;
    },
}

export default {
    namespaced: false,
    state,
    getters,
    actions,
    mutations
}