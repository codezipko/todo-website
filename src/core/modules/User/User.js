import {constants} from '../../constants/constants'
import axios from 'axios'
import {endpoints, baseURL, defaultHeaders} from "../../endpoints/endpoints";

const state = {
    user: {},
    logged: false
}

const getters = {
    getUser: (state) => {
        return state.user
    },

    getLoggedStatus: (state) => {
        return state.logged
    }
}

const actions = {
    loginUser({dispatch}, payload) {
        axios
            .post(baseURL + endpoints.LOGIN_USER.url.path(), {
                email: payload.email,
                password: payload.password
            })
            .then(function (response) {
                if (response) {
                    dispatch('setUser', response.data)
                    window.location.href = '/'
                }
            })
    },

    editUser({dispatch, rootState}, payload) {
        axios
            .put(baseURL + endpoints.UPDATE_USER.url.path(), {
                    user_id: payload.user_id,
                    name: payload.name,
                    role: payload.role
                },
                {
                    headers: {
                        defaultHeaders,
                        Authorization: `Bearer ${rootState.User.user.token}`
                    }
                })
            .then(function (response) {
                if (response) {
                    dispatch('logoutUser')
                }
            })
    },

    setUser({commit}, response) {
        commit(constants.user.mutations.SET, response)
        commit(constants.user.mutations.STATUS, true)
    },

    logoutUser({commit, rootState}) {
        axios
            .post(baseURL + endpoints.LOGOUT_USER.url.path(), null, {
                headers: {
                    defaultHeaders,
                    Authorization: `Bearer ${rootState.User.user.token}`
                }
            })
            .then(function (response) {
                if (response) {
                    commit(constants.user.mutations.SET, response)
                    commit(constants.user.mutations.STATUS, false)
                    window.location.reload();
                }
            })
    }
}

const mutations = {
    [constants.user.mutations.SET]
        (state, response) {
        state.user = response;
    },

    [constants.user.mutations.STATUS]
        (state, status) {
        state.logged = status
    }
}

export default {
    namespaced: false,
    state,
    getters,
    actions,
    mutations
}