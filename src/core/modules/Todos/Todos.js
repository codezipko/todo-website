import {constants} from '../../constants/constants'
import axios from 'axios'
import {endpoints, baseURL, defaultHeaders} from "../../endpoints/endpoints";

const state = {
    todos: [],
    status: '',
    sortBy: 'all',
    order: 'asc'
}

const getters = {
    todoList: (state) => {
        return state.todos
    },

    sortBy: (state) => {
        return state.sortBy
    },

    orderBy: (state) => {
        return state.order
    }
}

const actions = {
    getTodos({commit, dispatch, rootState}) {
        dispatch('setTodoListStatus', 'loading');
        axios
            .get(baseURL + endpoints.GET_TODOS_LIST.url.path(rootState.Todos.sortBy, rootState.Todos.order),
                {
                    headers: {
                        defaultHeaders,
                        Authorization: `Bearer ${rootState.User.user.token}`
                    }
                }
            )
            .then(function (response) {
                if (response) {
                    commit(constants.todos.mutations.GET, response.data)
                    dispatch('setTodoListStatus', 'success')
                }
            })
            .catch(function (error) {
                if (error) {
                    dispatch('setTodoListStatus', 'failed')
                }
            })
    },

    addTodo({commit, rootState}, payload) {
        axios
            .post(baseURL + endpoints.CREATE_TODO.url.path(), {
                task: payload.task,
                user_id: payload.user,
                status: payload.status
            }, {
                headers: {
                    defaultHeaders,
                    Authorization: `Bearer ${rootState.User.user.token}`
                }
            })
            .then((response) => response.data)

        commit(constants.todos.mutations.SET, payload)
    },

    setTodoListStatus({commit}, status) {
        commit(constants.todos.mutations.TODO_STATUS, status)
    },

    setProgressStatus({dispatch, rootState}, payload) {
        axios
            .put(baseURL + endpoints.UPDATE_TASK.url.path(),
                {
                    id: payload.id,
                    user_id: payload.user_id,
                    status: payload.status
                },
                {
                    headers: {
                        defaultHeaders,
                        Authorization: `Bearer ${rootState.User.user.token}`
                    }
                }
            )
            .then(function (response) {
                if (response) {
                    dispatch('getTodos')
                }
            })
    },

    sortStatus({commit, dispatch}, sorting) {
        commit(constants.todos.mutations.SORT, sorting);
        dispatch('getTodos')
    },

    orderTasks({commit, dispatch}, orderBy) {
        commit(constants.todos.mutations.ORDER, orderBy)
        dispatch('getTodos')
    }
}

const mutations = {
    [constants.todos.mutations.SET]
        (state, payload) {
        state.todos.push(payload)
    },

    [constants.todos.mutations.GET]
        (state, payload) {
        state.todos = payload;
    },

    [constants.todos.mutations.TODO_STATUS]
        (state, payload) {
        state.status = payload;
    },

    [constants.todos.mutations.SORT]
        (state, sorting) {
        state.sortBy = sorting
    },

    [constants.todos.mutations.ORDER]
        (state, orderBy) {
        state.order = orderBy
    }
}

export default {
    namespaced: false,
    state,
    getters,
    actions,
    mutations
}