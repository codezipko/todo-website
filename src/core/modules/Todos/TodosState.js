export const TodosState = {

    state: {
        todos: []
    },

    mutations: {
        incrementCounter(state, payload) {
            state.count += payload
        }
    },

    actions: {
        inrementAction({commit}, payload) {
            commit('incrementCounter', payload)
        }
    },

    getters: {
        counter(state) {
            return state.count
        }
    }

};